HDF5_DIR = /Users/venkatkrisshna/opt/hdf5
SZIP_DIR = /Users/venkatkrisshna/opt/szip
SILO_DIR = /Users/venkatkrisshna/opt/silo

SILO_INC = -I$(SILO_DIR)/include
SILO_LIB = -L$(SILO_DIR)/lib -lsiloh5 -L$(HDF5_DIR)/lib -lhdf5 -L$(SZIP_DIR)/lib -lsz -lz -lstdc++ #-L/usr/local/lib -lstdc++ -lz

F_SRC := $(wildcard *.f90)

F90 := mpifort #gfortran

FFLAGS := -g -fbounds-check -Wall -pedantic -fbacktrace

OBJECTS := $(C_SRC:%.c=%.o) $(F_SRC:%.f90=%.o)

run: silo_writeRead
	        ./pendulum_swing

silo_writeRead: $(OBJECTS)
	        $(F90) -o pendulum_swing *.o -lc $(SILO_LIB)

%.o: %.f90
	        $(F90) $(FFLAGS) $(SILO_INC) -c $< -o $@

clean:
	        rm -fr *.o pendulum_swing Visit
