! ================ !
!  PENDULUM SWING  !
! ================ !
! Venkata Krisshna

! Simulate the swinging of a pendulum
! Equipped with generation of a SILO output
! file that can be visualized on VisIt

program pendulum_swing

  implicit none
  include 'silo_f9x.inc'
  integer, parameter :: WP=kind(1.0d0)
  real(WP),parameter :: pi=3.141592653589793238_WP
  real(WP),parameter :: g=9.82182_WP ! Local (Bozeman,MT)

  ! Computational grid objects
  real(WP) :: Lx,Ly
  real(WP) :: dt,t_end=1.0_WP,p_end=1.0_WP
  integer  :: nx,ny
  real(WP) :: dx,dy
  integer  :: imin,imax,jmin,jmax
  real(WP),dimension(:),allocatable :: x,y,xm,ym
  real(WP) :: z(2)
  integer,dimension(:,:,:),allocatable :: bob
  character(len=20) f_end
  integer :: m

  ! Problem physics objects
  real(WP) :: m_P,L_P,r_B,h_P,a_P,f_P
  character(len=20) r,f

  print*,'================================='
  print*,' Welcome to the Pendulum Swinger '
  print*,'================================='
  print*,'    Author: Venkata Krisshna     '
  print*,''
  
  call init
  call step

contains

  ! ======================= !
  !  Initialize simulation  !
  ! ======================= !
  subroutine init
    implicit none

    call sim_init
    call silo_init
    call grid_init
    call bob_init
    
    return
  end subroutine init

  ! ============= !
  !  Main solver  !
  ! ============= !
  subroutine step
    implicit none
    integer :: t,t_total
    real(WP) :: p
    real(WP) :: theta_m,theta_0,theta_p
    
    ! Period and frequency
    p = 2.0_WP*pi*sqrt(real(L_P,WP)/g)
    print*,'Period of this pendulum is   ',p,'s'
    print*,'Frequency of this pendulum is',real(L_P,WP)/p,'Hz'
    
    ! Simulation end time
    if (f_end.eq.'period') t_end = real(p_end)*p
    
    ! Initial condition
    call initial_condition(theta_0)
    theta_m = theta_0
    
    t_total = int(t_end/dt)
    print*,'Iterating through timesteps...'
    do t=1,t_total
       call swing_step(theta_m,theta_0,theta_p,t)
       call bob_step(real(t,WP),theta_p)
       call silo_step(t)
       theta_m = theta_0
       theta_0 = theta_p
    end do
    print*,''
    print*,'     Simulation completed!'
    print*,''
    
    return
  end subroutine step
    
  ! ============ !
  !  Read input  !
  ! ============ !
  subroutine sim_init
    implicit none
    
    ! Problem Physics
    m_P = 1         ! Mass           (kg)
    L_P = 1         ! Length         (m)
    r_B = 0.05      ! Radius of bob  (m)
    h_P = 1         ! Release height (m)
    a_P = 5         ! Damping coefficient
    f_P = 10        ! Initial force  (N)
    r = ''          ! Initial release position (REST/right/left)
    f = ''          ! Direction of applied force (RIGHT/left)
    
    ! ComputatIonal Parameters
    nx = 100       ! Grid points in x
    ny = 100       ! Grid points in y
    Lx = 2.5_WP    ! Length of domain in x
    Ly = 2.5_WP    ! Length of domain in y
    dt = 0.1_WP    ! Timestep (s)
    f_end = 'time' ! Simulation end method
    t_end = 10     ! End simulation after time (s)
    p_end = 10     ! End simulation after # periods
    
    return
  end subroutine sim_init

  ! =============== !
  !  Initiate SILO  !
  ! =============== !
  subroutine silo_init
    implicit none
    
    call execute_command_line('rm Visit/step*') ! Erase existing Visit data
    call execute_command_line('mkdir -p Visit')
    
    return
  end subroutine silo_init

  ! =============== !
  !  Initiate grid  !
  ! =============== !
  subroutine grid_init
    implicit none
    integer :: i,j
    
    ! Index extents
    imin = 1
    imax = imin+nx-1
    jmin = 1
    jmax = jmin+ny-1

    ! Grid spacing
    dx = Lx/real(nx,WP)
    dy = Ly/real(ny,WP)
    
    ! Allocate arrays
    allocate(x(imin:imax+1))
    allocate(y(jmin:jmax+1))
    allocate(xm(imin:imax))
    allocate(ym(jmin:jmax))
    
    ! Create grid
    do i=1,nx+1
       x(i) = real(i-1,WP)*dx-0.5*Lx
    end do
    do j=1,ny+1
       y(j) = real(j-1,WP)*dy+0.5*Ly
    end do
    z = [0,1]*1e-5
    do i=1,nx
       xm(i) = 0.5_WP*(x(i)+x(i+1))
    end do
    do i=1,ny
       ym(i) = 0.5_WP*(y(i)+y(i+1))
    end do
    
    return
  end subroutine grid_init
  
  ! =============================== !
  !  Place bob at initial position  !
  ! =============================== !
  subroutine bob_init
    implicit none
    real(WP) :: theta_0
    
    ! Determine bob size
    m = int(r_B/dx)
    
    ! Allocate bob
    allocate(bob(imin-m:imax+m,jmin-m:jmax+m,1))
    bob = 0
    
    call initial_condition(theta_0)
    call bob_step(0.0_WP,theta_0)
    call silo_step(0)
    
    return
  end subroutine bob_init

  ! ==================== !
  !  Initial condition  !
  ! ==================== !
  subroutine initial_condition(t0)
    implicit none
    real(WP),intent(out) :: t0

    select case(r)
    case ('left')
       t0 = -acos(1.0_WP-real(h_P,WP)/real(L_P,WP))
    case ('right')
       t0 =  acos(1.0_WP-real(h_P,WP)/real(L_P,WP))
    case ('rest')
       if (real(f_P,WP).eq.0.0_WP) print*,'No initial force!'
       t0 = 0.0_WP
    case default
       if (real(f_P,WP).eq.0.0_WP) print*,'No initial force!'
       t0 = 0.0_WP
    end select
    
    return
  end subroutine initial_condition

  ! ================= !
  !  Swing iteration  !
  ! ================= !
  subroutine swing_step(theta_m,theta_0,theta_p,t)
    implicit none
    real(WP),intent(in) :: theta_m,theta_0
    real(WP),intent(out) :: theta_p
    integer :: t
    
    ! Governing equation of a damped pendulum
    if (t.eq.1 .and. real(f_P).ne.0.0_WP) then
       select case(f)
       case ('left')
          theta_p = -(real(f_P,WP)*(dt**2)/real(m_P,WP)) - (theta_m - 2*theta_p)
       case ('right')
          theta_p = +(real(f_P,WP)*(dt**2)/real(m_P,WP)) - (theta_m - 2*theta_p)
       case default
          theta_p = +(real(f_P,WP)*(dt**2)/real(m_P,WP)) - (theta_m - 2*theta_p)
       end select
    else
       theta_p = (-g*sin(theta_0)*(dt**2)/real(L_P,WP) - (theta_m - 2*theta_0) + a_P*theta_0*dt**2)/&
            (a_P*dt**2+1)
    end if
    
    return
  end subroutine swing_step
  
  ! ========================== !
  !  Place bob after timestep  !
  ! ========================== !
  subroutine bob_step(t,theta_n)
    implicit none
    real(WP),intent(in) :: t,theta_n
    real(WP) :: t_sim
    real(WP) :: L_Px,L_Py
    integer :: i,j
    
    ! Compute current time in simulation
    t_sim = dt*t

    ! ID current Lx, Ly of bob
    L_Px = real(L_P,WP)*sin(theta_n)
    L_Py = Ly-real(L_P,WP)*cos(theta_n)
    
    ! Place bob
    bob = 0
    do j = jmin,jmax
       if (ym(j).ge.L_Py) exit
    end do
    do i = imin,imax
       if (xm(i).ge.L_Px) exit
    end do
    bob(i-m:i+m,j-m:j+m,1) = 1
    
    return
  end subroutine bob_step

  ! ==================== !
  !  Write to SILO file  !
  ! ==================== !
  subroutine silo_step(t_sim)
    implicit none
    integer,intent(in) :: t_sim
    integer dbfile, ierr
    character(len=50) :: siloname

    ! Create the silo database
    write(siloname,'(A,I0.3,A)') 'Visit/step',t_sim,'.silo'
    ierr = dbcreate(siloname, len_trim(siloname), DB_CLOBBER, DB_LOCAL,"Silo database", 13, DB_HDF5, dbfile)
    if(dbfile.eq.-1) print *,'Could not create Silo file!'
    ierr = dbclose(dbfile)
    ierr = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)
    ierr = dbputqm(dbfile,"Grid",4,'xc',2,'yc',2,'zc',2,x,y,z,(/nx+1,ny+1,2/),&
         3,DB_DOUBLE,DB_COLLINEAR,DB_F77NULL,ierr)
    ierr = dbputqv1(dbfile,'Bob',3,"Grid",4,bob(imin:imax,jmin:jmax,1),(/nx,ny,1/),3,DB_F77NULL,0,&
         DB_INT,DB_ZONECENT,DB_F77NULL,ierr)
    ierr = dbclose(dbfile)
    
    return
  end subroutine silo_step

  ! ================= !
  !  Print bob field  !
  ! ================= !
  subroutine print_bob
    implicit none
    integer :: j

    do j = jmax+1,jmin-1,-1
       print*,bob(:,j,1)
    end do

    return
  end subroutine print_bob

end program pendulum_swing
